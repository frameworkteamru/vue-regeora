
export class Marker {
  constructor ({ id, latLng, layerId }) {
    this.id = id || -1
    this.latLng = latLng || []
    this.layerId = layerId || -1
    return this
  }
}
