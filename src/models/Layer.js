
export class Layer {
  constructor ({ id, text, markers }) {
    this.id = id || -1
    this.text = text || ''
    this.markers = markers || []
    return this
  }
}
