export const mockPromise = (mockData, resolveProbability, minTime, maxTime) => {
  if (resolveProbability === undefined || resolveProbability === null) {
    resolveProbability = 1
  }

  if (minTime > maxTime) {
    console.error('[mockPromise]: minTime cannot be greater than maxTime')
  }

  // Creating mock data in case none is passed in
  var data = mockData || 'Resolved Value'

  // Picks a random time between minTime and maxTime
  var timeOut = Math.floor(Math.random() * ((maxTime || 1000) - (minTime || 1) + 1)) + (minTime || 1)

  // Creates a new promise. If the resolve probability is higher than
  // Math.random(), the promise will resolve. If not, it will be rejected.
  return new Promise((resolve, reject) => {
    if (Math.random() < resolveProbability) {
      setTimeout(() => {
        resolve(data)
      }, timeOut)
    } else {
      setTimeout(() => {
        reject(new Error('Promise failed to resolve. ' + timeOut + 'ms'))
      }, timeOut)
    }
  })
}
