import { eachObject } from './helpers'
import { Layer } from '../models'

export const removeById = (state, id) => {
  let entitiesById = {}
  eachObject(state, (item, key) => {
    if ((+key) === (+id)) {
      return null
    }
    entitiesById[key] = state[key]
  })
  return entitiesById
}

export const layerWithMarker = (state, payload) => {
  let id = payload.id
  let layerId = payload.layerId
  let oldLayer = state[layerId]
  let layer = new Layer({...oldLayer, markers: [...oldLayer.markers]})
  layer.markers.push(id)
  return layer
}

export const layerWithoutMarker = (state, payload) => {
  var id = payload.id
  var layerId = payload.layerId
  var markers = state[layerId].markers.filter((marker) => +id !== +marker)
  var layer = new Layer({...state[layerId], markers})
  return layer
}
