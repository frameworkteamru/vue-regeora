import { mockPromise } from '@/utils'

const data = require('./mock-data')

export function getAllLayers () {
  return mockPromise(data, 0.8, 1000, 2000)
}

export function saveLayer (body) {
  return mockPromise('saved', 0.8, 1000, 2000)
}
