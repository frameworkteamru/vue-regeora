module.exports = [
  {
    'id': 1,
    'text': 'First',
    'markers': [
      {
        'id': 1,
        'latLng': [51.502652102264484, -0.09235382080078126],
        'layer_id': 1
      },
      {
        'id': 2,
        'latLng': [51.48651406499528, -0.0748443603515625],
        'layer_id': 1
      }
    ]
  },
  {
    'id': 2,
    'text': 'Second',
    'markers': []
  }
]
