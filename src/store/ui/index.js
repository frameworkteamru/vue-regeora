import map from './map'
import modal from './modal'

export default {
  namespaced: true,
  modules: {
    map,
    modal
  }
}
