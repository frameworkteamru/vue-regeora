export default {
  show (state, payload) {
    state.shown = true
  },
  hide (state, payload) {
    state.shown = false
  },
  setEditable (state, payload) {
    state.editable = payload
  }
}
