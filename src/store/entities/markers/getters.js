import { notEmptyObject } from '@/utils'

export const markers = state => state.data
export const markersNotEmpty = state => notEmptyObject(state.data)
