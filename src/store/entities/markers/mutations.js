import { removeById } from '@/utils'

export default {
  set (state, payload) {
    state.data = payload
  },
  add (state, payload) {
    state.data = {...state.data, [payload.id]: payload}
  },
  remove (state, payload) {
    state.data = removeById(state.data, payload.id)
  }
}
