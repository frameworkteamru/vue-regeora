import { Marker } from '@/models'
import { getMaxKey, layerWithMarker, layerWithoutMarker } from '@/utils'

export const addMarker = ({ commit, getters, rootGetters }, payload) => {
  var id = getMaxKey(getters.markers) + 1
  var marker = new Marker({...payload, id})
  commit('add', marker)
  commit(
    'entities/layers/add',
    layerWithMarker(rootGetters['entities/layers/layers'], marker),
    {root: true}
  )
  return marker
}

export const removeMarker = ({ commit, getters, rootGetters }, payload) => {
  commit('remove', payload)
  commit(
    'entities/layers/add',
    layerWithoutMarker(rootGetters['entities/layers/layers'], payload),
    {root: true}
  )
}
