import { removeById } from '@/utils'

export default {
  get (state, payload) {
    state.fetching = true
  },
  getSuccess (state, payload) {
    state.data = payload
    state.fetching = false
    state.loaded = true
  },
  getError (state, payload) {
    state.fetching = false
    state.loaded = false
  },
  setActive (state, payload) {
    state.active = payload
  },
  add (state, payload) {
    state.data = {...state.data, [payload.id]: payload}
  },
  remove (state, payload) {
    state.data = removeById(state.data, payload.id)
  },
  save (state, payload) {
    //
  },
  saveSuccess (state, payload) {
    //
  },
  saveError (state, payload) {
    //
  }
}
