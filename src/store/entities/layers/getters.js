export const layers = state => state.data
export const loaded = state => state.loaded
export const fetching = state => state.fetching

export const active = state => state.active

export const editableLayer = (state, getters, rootState, rootGetters) => {
  var id = rootGetters['ui/modal/editable']
  if (id < 0) return false
  var editable = getters.layers[id]
  return editable || false
}
