import { getAllLayers, saveLayer as apiSaveLayer } from '@/api'
import { Layer, Marker } from '@/models'
import { normalizeLayers, denormalizeLayers, eachObject, getMaxKey } from '@/utils'

export const getLayers = ({ commit }, payload) => {
  commit('get')
  getAllLayers().then(
    getLayersSuccess({ commit }),
    (error) => commit('getError', error)
  )
}

const getLayersSuccess = ({ commit }) => (response) => {
  let normalizedData = normalizeLayers(response)
  let entities = normalizedData.entities
  let layers = {}
  eachObject(entities.layers, (layer, key) => {
    layers[key] = new Layer({...layer})
  })
  let markers = {}
  eachObject(entities.markers, (marker, key) => {
    markers[key] = new Marker({...marker, layerId: marker.layer_id})
  })
  commit('getSuccess', layers)
  commit('entities/markers/set', markers, {root: true})
}

export const addLayerByText = ({ commit, getters }, text) => {
  var id = getMaxKey(getters.layers) + 1
  var layer = new Layer({ id, text })
  commit('add', layer)
}

export const editLayerText = ({ commit }, payload) => {
  var text = payload.text
  var layer = new Layer({ ...payload.layer, text })
  commit('add', layer)
}

export const destroyLayer = ({ commit, getters }, payload) => {
  if (+payload.id === +getters.active) {
    commit('setActive', -1)
  }
  commit('remove', payload)
}

export const saveLayer = ({ commit, getters, rootGetters }, payload) => {
  var layers = getters.layers
  var markers = rootGetters['entities/markers/markers']
  var id = payload.id

  var body = denormalizeLayers([id], { markers, layers })
  commit('save', body)

  apiSaveLayer(body).then(
    (response) => commit('saveSuccess', response),
    (error) => commit('saveError', error)
  )
}
