import layers from './layers'
import markers from './markers'

export default {
  namespaced: true,
  modules: {
    layers,
    markers
  }
}
