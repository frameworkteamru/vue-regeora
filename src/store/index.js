import Vue from 'vue'
import Vuex from 'vuex'

import entities from './entities'
import ui from './ui'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    entities,
    ui
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
